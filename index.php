<?php

require 'config/config.php';

include('Shop.php');

$holidays 	= include 'config/holidays.config.php';
$times 		= include 'config/times.config.php';

$routes = [
	'404'			=> 'errors/404.html.twig',
	'/'				=> 'pages/index.html.twig',
	'/opening_times'=> 'pages/opening_times.html.twig',
	'/holidays'		=> 'pages/holidays.html.twig',
	'/isOpen'		=> 'pages/status.html.twig',
	'/isClosed'		=> 'pages/status.html.twig',
	'/nextOpen'		=> 'pages/status.html.twig',
	'/nextClosed'	=> 'pages/status.html.twig',
];
$url 		= $_SERVER['REQUEST_URI'];
$urlMethod 	= str_replace('/', '', $url);
$route = $routes[$url];

if (!array_key_exists($url, $routes)) {
	$route 	= $routes['404'];
}

$shop 			= new Shop();
$statusQuery 	= null;
if (method_exists($shop, $urlMethod)) {
	$statusQuery = $shop->{$urlMethod}(null, $times);
}

echo $twig->render($route, array(
	'holidays'		=> $holidays,
	'times'			=> $times,
	'currDay'		=> $currDay,
	'shop'			=> $shop,
	'statusQuery'	=> $statusQuery,
	'urlMethod'		=> $urlMethod,
));