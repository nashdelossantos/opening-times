<?php

class Shop
{
    public function getStatus(array $times = [])
    {
        $date           = new \DateTime();
        $day            = $date->format('l');
        $time           = $date->format('G:i');

        $currentStatus  = null;

        if (array_key_exists($day, $times)) {
            foreach ($times[$day] as $timex => $status) {
                if ($timex < $time) {
                    $currentStatus = $status;
                    continue;
                }
                if ($timex > $time) {
                    break;
                }
            }
        }

        return $currentStatus;
    }

    /**
     * Is the shop open on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     * @return boolean
     */
    public function isOpen(DateTime $dt = null, array $times = [])
    {
        if (!$dt) {
            $dt = new \DateTime();
        }

        $day            = $dt->format('l');
        $time           = $dt->format('G:i');

        $status  = false;

        if (array_key_exists($day, $times)) {
            foreach ($times[$day] as $timex => $stat) {
                if ($timex < $time) {
                    $status = $stat['status'] == 'open' ? true : false;
                    continue;
                }
                if ($timex > $time) {
                    break;
                }
            }
        }

        return $status;
    }


    /**
     * Is the shop closed on the provided date/time
     * If provided a DateTime object, check relative to that, otherwise use now
     *
     * @param DateTime $dt
     * @return boolean
     */
    public function isClosed(DateTime $dt = null, array $times = [])
    {
        if (!$dt) {
            $dt = new \DateTime();
        }

        $day            = $dt->format('l');
        $time           = $dt->format('G:i');

        $status  = false;

        if (array_key_exists($day, $times)) {
            foreach ($times[$day] as $timex => $stat) {
                if ($timex < $time) {
                    $status = $stat['status'] == 'closed' ? true : false;
                    continue;
                }
                if ($timex > $time) {
                    break;
                }
            }
        }

        return $status;
    }


    /**
     * At what date/time will the shop next be open
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already open, return the provided datetime/now
     *
     * @param DateTime $dt
     * @return DateTime
     */
    public function nextOpen(DateTime $dt = null, array $times = [])
    {
        if (!$dt) {
            $dt = new \DateTime();
        }

        $day            = $dt->format('l');
        $time           = $dt->format('G:i');
        $hasOpening     = false;
        $nowOrFuture    = false;
        $result         = [];

        $nextDay        = $dt->modify('tomorrow');

        $x = 0;
        while ($x <=1) {
            foreach ($times as $tday => $timex) {
                // if it gets here that means shop is closed all days
                if ($tday == $nextDay->format('l') && $x == 1) {
                    break;
                }

                if ($tday == $day) {
                    $nowOrFuture = true;
                }

                // start from today and forward
                if ($nowOrFuture) {
                    foreach ($timex as $dayTime => $status) {
                        // exclude earlier today
                        if ($tday == $day && $dayTime < $time) {
                            continue;
                        }
                        if ($status['status'] == 'open') {
                            $result = [
                                'day'   => $tday,
                                'time'  => $dayTime
                            ];
                            break;
                        }
                    }
                    if (count($result) > 0) {
                        break;
                    }
                }
            }
            $x++;
        }

        return $result;
    }


    /**
     * At what date/time will the shop next be closed
     * If provided a DateTime object, check relative to that, otherwise use now
     * If the shop is already closed, return the provided datetime/now
     *
     * @param DateTime $dt
     * @return DateTime
     */
    public function nextClosed(DateTime $dt = null, array $times = [])
    {
        if (!$dt) {
            $dt = new \DateTime();
        }

        $day            = $dt->format('l');
        $time           = $dt->format('G:i');
        $hasOpening     = false;
        $nowOrFuture    = false;
        $result         = [];

        $nextDay        = $dt->modify('tomorrow');

        $x = 0;
        while ($x <=1) {
            foreach ($times as $tday => $timex) {

                if ($tday == $nextDay->format('l') && $x == 1) {
                    break;
                }

                if ($tday == $day) {
                    $nowOrFuture = true;
                }

                // start from today and forward
                if ($nowOrFuture) {
                    foreach ($timex as $dayTime => $status) {
                        // exclude earlier today
                        if ($tday == $day && $dayTime < $time) {
                            continue;
                        }
                        if ($status['status'] == 'closed') {
                            $result = [
                                'day'   => $tday,
                                'time'  => $dayTime
                            ];
                            break;
                        }
                    }
                    if (count($result) > 0) {
                        break;
                    }
                }
            }
            $x++;
        }

        return $result;
    }

}