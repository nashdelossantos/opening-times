<?php

date_default_timezone_set("Europe/London");

require 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__ . '/../view');
$twig = new Twig_Environment($loader, array(
    'cache' => false,
));