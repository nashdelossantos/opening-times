<?php
    return [
        'Monday' => [
            '09:00' => [
                'status'    => 'open',
                'note'      => 'Open',
            ],
            '12:20' => [
                'status'    => 'closed',
                'note'      => 'Close for Lunch'
            ],
            '13:30' => [
                'status'    => 'open',
                'note'      => 'Return from Lunch'
            ],
            '17:00' => [
                'status'    => 'closed',
                'note'      => 'Close for the day'
            ]
        ],
        'Tuesday' => [
            '09:00' => [
                'status'    => 'open',
                'note'      => 'Open'
            ],
            '12:20' => [
                'status'    => 'closed',
                'note'      => 'Close for Lunch'
            ],
            '13:30' => [
                'status'    => 'open',
                'note'      => 'Return from Lunch'
            ],
            '17:00' => [
                'status'    => 'closed',
                'note'      => 'Close for the day'
            ]
        ],
        'Wednesday' => [
            '09:00' => [
                'status'    => 'open',
                'note'      => 'Open'
            ],
            '12:20' => [
                'status'    => 'closed',
                'note'      => 'Close for Lunch'
            ],
            '13:30' => [
                'status'    => 'open',
                'note'      => 'Return from Lunch'
            ],
            '17:00' => [
                'status'    => 'closed',
                'note'      => 'Close for the day'
            ]
        ],
        'Thursday' => [
            '09:00' => [
                'status'    => 'open',
                'note'      => 'Open'
            ],
            '12:30' => [
                'status'    => 'closed',
                'note'      => 'Close for Lunch'
            ],
            '13:30' => [
                'status'    => 'open',
                'note'      => 'Return from Lunch'
            ],
            '17:00' => [
                'status'    => 'closed',
                'note'      => 'Close for the day'
            ]
        ],
        'Friday' => [
            '09:00' => [
                'status'    => 'open',
                'note'      => 'Open'
            ],
            '12:30' => [
                'status'    => 'closed',
                'note'      => 'Close for Lunch'
            ],
            '13:30' => [
                'status'    => 'open',
                'note'      => 'Return from Lunch'
            ],
            '17:00' => [
                'status'    => 'closed',
                'note'      => 'Close for the day'
            ]
        ],
        'Saturday' => [ // Closed on Weekends

        ],
        'Sunday' => [  // Closed on Weekends

        ],
    ];